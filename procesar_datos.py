#!/usr/bin/env python

### https://software.ecmwf.int/wiki/display/WEBAPI/Access+ECMWF+Public+Datasets
### https://api.ecmwf.int/v1/key/
### http://apps.ecmwf.int/webmars/joblist/
### https://software.ecmwf.int/wiki/display/WEBAPI/Web-API+Troubleshooting
### https://software.ecmwf.int/wiki/display/WEBAPI/Python+ERA-interim+examples

from ecmwfapi import ECMWFDataServer
server = ECMWFDataServer()

ruta_ficheros = "/mnt/dostb2/fuendetodos/DATOS/paquete_sequia/descargas/"

### Prueba 1 día viento
# server.retrieve({
#     "class": "ei",
#     "dataset": "interim",
#     "date": "1979-01-01/to/1979-01-01",
#     "expver": "1",
#     "grid": "0.25/0.25",
#     "levtype": "sfc",
#     "param": "49.128",
#     "step": "3",
#     "stream": "oper",
#     "time": "00:00:00/12:00:00",
#     "type": "fc",
#     "area": "73.5/-27/33/45",
#     'format': "netcdf",
#     "target": "wind_19790101.nc",
# })

MAXWIND = 0
TEMP = 1
DTEMP1 = 2
SNOWFALL = 3
SNOWMELT = 4
HCLOUD = 5
LCLOUD1 = 6
LCLOUD2 = 7
SUNSHINE = 8
SOLAR = 9
UWIND1 = 10
UWIND2 = 11
VWIND1 = 12
VWIND2 = 13
DTEMP2 = 14
SNOWDEPTH1 = 15
SNOWDEPTH2 = 16

params = \
    [ \
        "49.128", # 10 metre wind gust since previous post-processing \
        "167.128", # 2 metre temperature \
        "168.128", # 2 metre dewpoint temperature \
        "144.128", # Snowfall \
        "45.128", # Snowmelt \
        "188.128", # High cloud cover \
        "186.128", # Low cloud cover \
        "186.128", # Low cloud cover \
        "189.128", # Sunshine duration \
        "169.128", # Surface solar radiation downwards \
        "165.128", #  10 metre U wind component \
        "165.128", #  10 metre U wind component \
        "166.128", #  10 metre V wind component \
        "166.128", #  10 metre V wind component \
        "168.128", # 2 metre dewpoint temperature \
        "141.128", #  Snow depth \
        "141.128", #  Snow depth \
        "164.128", #  Total cloud cover \
    ]

targets =  \
    [ \
        "max_wind", # 10 metre wind gust since previous post-processing \
        "temperature", # 2 metre temperature \
        "dewpoint_temperature_1", # 2 metre dewpoint temperature \
        "snowfall", # Snowfall \
        "snowmelt", # Snowmelt \
        "high_cloud", # High cloud cover \
        "low_cloud_1", # Low cloud cover \
        "low_cloud_2", # Low cloud cover \
        "sunshine", # Sunshine duration \
        "radiation", # Surface solar radiation downwards \
        "u_wind_1", # 10 metre U wind component \
        "u_wind_2", # 10 metre U wind component \
        "v_wind_1", # 10 metre V wind component \
        "v_wind_2", # 10 metre V wind component \
        "dewpoint_temperature_2", # 2 metre dewpoint temperature \
        "snow_depth_1", #  Snow depth \
        "snow_depth_2", #  Snow depth \
    ] 

times =  \
    [ \
        "00:00:00/12:00:00", # 10 metre wind gust since previous post-processing \
        "00:00:00/06:00:00/12:00:00/18:00:00", # 2 metre temperature \
        "00:00:00/06:00:00/12:00:00/18:00:00", # 2 metre dewpoint temperature \
        "00:00:00/12:00:00", # Snowfall \
        "00:00:00/06:00:00/12:00:00/18:00:00", # Snowmelt \
        "00:00:00/06:00:00/12:00:00/18:00:00", # High cloud cover \
        "00:00:00/06:00:00/12:00:00/18:00:00", # Low cloud cover \
        "00:00:00/12:00:00", # Low cloud cover \
        "00:00:00/12:00:00", # Sunshine duration \
        "00:00:00/06:00:00/12:00:00/18:00:00", # Surface solar radiation downwards \
        "00:00:00/06:00:00/12:00:00/18:00:00", # 10 metre U wind component \
        "00:00:00/12:00:00", # 10 metre U wind component \
        "00:00:00/06:00:00/12:00:00/18:00:00", # 10 metre V wind component \
        "00:00:00/12:00:00", # 10 metre V wind component \
        "00:00:00/12:00:00", # 2 metre dewpoint temperature \
        "00:00:00/06:00:00/12:00:00/18:00:00", #  Snow depth \
        "00:00:00/12:00:00", #  Snow depth \
    ] 

steps =  \
    [ \
        "3/6/9/12", # 10 metre wind gust since previous post-processing \
        "0/3/6/9/12", # 2 metre temperature \
        "0", # 2 metre dewpoint temperature \
        "3/6/9/12", # Snowfall \
        "0/3/6/9/12", # Snowmelt \
        "0/3/6/9/12", # High cloud cover \
        "0", # Low cloud cover \
        "3/6/9/12", # Low cloud cover \
        "3/6/9/12", # Sunshine duration \
        "0/3/6/9/12", # Surface solar radiation downwards \
        "0", # 10 metre U wind component \
        "3/6/9/12", # 10 metre U wind component \
        "0", # 10 metre V wind component \
        "3/6/9/12", # 10 metre V wind component \
        "3/6/9/12", # 2 metre dewpoint temperature \  
        "0", #  Snow depth \
        "3/6/9/12", #  Snow depth \
    ] 

types =  \
    [ \
        "fc", # 10 metre wind gust since previous post-processing \
        "fc", # 2 metre temperature \
        "an", # 2 metre dewpoint temperature \
        "fc", # Snowfall \
        "fc", # Snowmelt \
        "fc", # High cloud cover \
        "an", # Low cloud cover \
        "fc", # Low cloud cover \
        "fc", # Sunshine duration \
        "fc", # Surface solar radiation downwards \
        "an", # 10 metre U wind component \
        "fc", # 10 metre U wind component \
        "an", # 10 metre V wind component \
        "fc", # 10 metre V wind component \
        "fc", # 2 metre dewpoint temperature \
        "an", #  Snow depth \
        "fc", #  Snow depth \
    ]

range_dates = "1979-01-01/to/2018-01-01"
# range_dates = "1979-01-01/to/1979-01-02"

i = SNOWDEPTH2
# param = params[i]
# target = targets[i]

# Diario actualidad
# http://apps.ecmwf.int/datasets/data/interim-full-daily/levtype=sfc/
# for i in range(len(param)):
    server.retrieve({
        "class": "ei",
        "dataset": "interim",
        "date": range_dates,
        "expver": "1",
        "grid": "0.25/0.25",
        "levtype": "sfc",
        "param": params[i],
        "step": steps[i],
        "stream": "oper",
        "time": times[i],
        "type": types[i],
        "area": "73.5/-27/33/45",
        'format': "netcdf",
        "target": ruta_ficheros + targets[i] + ".nc",
    })

# # Diario siglo XX
# # http://apps.ecmwf.int/datasets/data/era20c-daily/levtype=sfc/type=an/
# server.retrieve({
#     "class": "e2",
#     "dataset": "era20c",
#     "date": "1900-01-01/to/2010-12-31",
#     "expver": "1",
#     "levtype": "sfc",
#     "param": params[i],
#     "stream": "oper",
#     "time": "00:00:00/03:00:00/06:00:00/09:00:00/12:00:00/15:00:00/18:00:00/21:00:00",
#     "type": "an",
#     'format': "netcdf",
#     "target": ruta_ficheros + targets[i] + "_XX.nc",
# })


# # Mensual siglo XX
# # http://apps.ecmwf.int/datasets/data/era20cm-edmm/levtype=sfc/
# server.retrieve({
#     "class": "em",
#     "dataset": "era20cm",
#     "date": "1979-01-01/to/2017-12-31",
#     "expver": "1",
#     "levtype": "sfc",
#     "number": "0",
#     "param": params[i],
#     "step": "3",
#     "stream": "edmm",
#     "time": "00:00:00",
#     "type": "fc",
#     'format': "netcdf",
#     "target": ruta_ficheros + targets[i] + ".nc",
# })

# # Mensual actualidad
# # http://apps.ecmwf.int/datasets/data/interim-full-moda/levtype=sfc/
# # Como diario con 
# # "stream": "moda",
# server.retrieve({
#     "class": "ei",
#     "dataset": "interim",
#     "date": "19800101/19800201/19800301/19800401/19800501/19800601/19800701/19800801/19800901/19801001/19801101/19801201",
#     "expver": "1",
#     "grid": "0.75/0.75",
#     "levtype": "sfc",
#     "param": "66.162",
#     "stream": "moda",
#     "type": "an",
#     "target": "output",
# })

quit()
